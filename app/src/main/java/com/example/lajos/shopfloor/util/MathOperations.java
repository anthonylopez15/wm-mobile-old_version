package com.example.lajos.shopfloor.util;

import com.example.lajos.shopfloor.network.pojo.WorkGroupMonitorizationPojo;

import java.util.ArrayList;

public class MathOperations {

    public void removeZeroPerformance(ArrayList<WorkGroupMonitorizationPojo> selectedselectedGroupLinesPerformanceOrder) {

        ArrayList<WorkGroupMonitorizationPojo> temp = new ArrayList<>();

        for (WorkGroupMonitorizationPojo item : selectedselectedGroupLinesPerformanceOrder) {
            if (!item.getIndiceOEE().equals("0.00")) {
                temp.add(item);
            }
        }
        selectedselectedGroupLinesPerformanceOrder.clear();
        selectedselectedGroupLinesPerformanceOrder.addAll(temp);
    }
}