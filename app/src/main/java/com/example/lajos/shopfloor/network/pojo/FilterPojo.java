package com.example.lajos.shopfloor.network.pojo;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FilterPojo {

    @SerializedName("dtReferencia")
    @Expose
    private String dtReferencia;
    @SerializedName("idTurno")
    @Expose
    private Integer idTurno;
    @SerializedName("filtroOp")
    @Expose
    private Integer filtroOp;
    @SerializedName("cdGt")
    @Expose
    private String cdGt;
    @SerializedName("turnoAtual")
    @Expose
    private Boolean turnoAtual;
    @SerializedName("listaFiltroPosto")
    @Expose
    private List<Object> listaFiltroPosto = null;
    @SerializedName("listaFiltroIdGt")
    @Expose
    private List<Integer> listaFiltroIdGt = null;

    public String getDtReferencia() {
        return dtReferencia;
    }

    public void setDtReferencia(String dtReferencia) {
        this.dtReferencia = dtReferencia;
    }

    public Integer getIdTurno() {
        return idTurno;
    }

    public void setIdTurno(Integer idTurno) {
        this.idTurno = idTurno;
    }

    public Integer getFiltroOp() {
        return filtroOp;
    }

    public void setFiltroOp(Integer filtroOp) {
        this.filtroOp = filtroOp;
    }

    public String getCdGt() {
        return cdGt;
    }

    public void setCdGt(String cdGt) {
        this.cdGt = cdGt;
    }

    public Boolean getTurnoAtual() {
        return turnoAtual;
    }

    public void setTurnoAtual(Boolean turnoAtual) {
        this.turnoAtual = turnoAtual;
    }

    public List<Object> getListaFiltroPosto() {
        return listaFiltroPosto;
    }

    public void setListaFiltroPosto(List<Object> listaFiltroPosto) {
        this.listaFiltroPosto = listaFiltroPosto;
    }

    public List<Integer> getListaFiltroIdGt() {
        return listaFiltroIdGt;
    }

    public void setListaFiltroIdGt(List<Integer> listaFiltroIdGt) {
        this.listaFiltroIdGt = listaFiltroIdGt;
    }

}
