package com.example.lajos.shopfloor.activity;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Handler;
import android.speech.RecognizerIntent;
import android.speech.tts.TextToSpeech;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.lajos.shopfloor.R;
import com.example.lajos.shopfloor.network.calls.WmApiCalls;
import com.example.lajos.shopfloor.network.pojo.AnalyzesFilterPost;
import com.example.lajos.shopfloor.network.pojo.AnalyzesFilterPostPojo;
import com.example.lajos.shopfloor.network.service.WmService;
import com.example.lajos.shopfloor.network.pojo.UserLoginPojo;
import com.example.lajos.shopfloor.network.pojo.UserLoginResponse;
import com.example.lajos.shopfloor.util.UserSession;
import com.leo.simplearcloader.ArcConfiguration;
import com.leo.simplearcloader.SimpleArcDialog;

import java.util.ArrayList;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LoginActivity extends AppCompatActivity {

    String username;
    String password;
    UserSession session;
    SimpleArcDialog loadingDialog;
    static int teste = 0;
    protected static TextToSpeech ttobj;
    protected static final int RESULT_SPEECH = 1;
    EditText usernameEdit;
    EditText passwordEdit;
    ImageButton voiceButton;
    Button loginButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Context context = getApplicationContext();
        session = new UserSession(getApplicationContext());
        final WmApiCalls calls = new WmApiCalls(context, LoginActivity.this);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        ttobj = new TextToSpeech(getApplicationContext(),
                new TextToSpeech.OnInitListener() {
                    @Override
                    public void onInit(int status) {
                        if (status != TextToSpeech.ERROR) {
                            ttobj.setLanguage(new Locale("pt", "br"));
                        }
                    }
                });

        calls.checkLoginStatus(session, ttobj);

        usernameEdit = findViewById(R.id.user_login);
        passwordEdit = findViewById(R.id.user_password);
        loginButton = findViewById(R.id.login_button);
        voiceButton = findViewById(R.id.voice_button);


        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //username = usernameEdit.getText().toString();
                //password = passwordEdit.getText().toString();
                //calls.userLogin(username, password, session);
                calls.userLogin("map", "SistemaInsert", session, ttobj);
            }
        });

        voiceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (teste == 0) {
                    teste = 1;
                }
                readMicrofone();
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case RESULT_SPEECH: {
                if (resultCode == RESULT_OK && null != data) {
                    ArrayList<String> text = data
                            .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    String texto = text.get(0);
                    falar(texto);
                }
                break;
            }
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode == KeyEvent.KEYCODE_HEADSETHOOK){
            voiceButton.performClick();
            return true;
        }
        return false;
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_SPACE) {
            voiceButton.performClick();
            return true;
        }
        return super.onKeyUp(keyCode, event);
    }

    public void falar(String texto) {
        if (texto != null && !texto.isEmpty()) {

            if (texto.contains("bypass")) {

                try {
                    Thread.sleep(5);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                ttobj.speak("O componente xyz123 na máquina cp3,apresenta problema no feeder 5n343 na posição 72. É necessário Trocá-lo imediatamente.", TextToSpeech.QUEUE_FLUSH, null);
            }

            else if (texto.contains("login") &&
                    texto.contains("Júnior")) {
                try {
                    Thread.sleep(5);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                usernameEdit.setText("junior");
            }

            else if (texto.contains("senha") &&
                    texto.contains("teste") &&
                    texto.contains("2018")){
                try {
                    Thread.sleep(5);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                passwordEdit.setText("teste2018");
            }

            else if (texto.contains("fazer") &&
                    texto.contains("login")){
                try {
                    Thread.sleep(5);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                loginButton.performClick();
            }

            else {
                try {
                    ttobj.speak("Desculpa, não entendi.", TextToSpeech.QUEUE_FLUSH, null);

                    Thread.sleep(3);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }


            }
        }
    }

    public void readMicrofone() {
        Intent intent = new Intent(
                RecognizerIntent.ACTION_RECOGNIZE_SPEECH);

        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, "en-US");

        try {
            startActivityForResult(intent, RESULT_SPEECH);

        } catch (ActivityNotFoundException a) {
            Toast t = Toast.makeText(this,
                    "Ops! Your device doesn't support Speech to Text",
                    Toast.LENGTH_SHORT);
            t.show();
        }
    }

}
