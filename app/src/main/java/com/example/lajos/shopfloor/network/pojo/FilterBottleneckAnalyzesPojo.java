package com.example.lajos.shopfloor.network.pojo;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FilterBottleneckAnalyzesPojo {

    @SerializedName("dtReferencia")
    @Expose
    private String dtReferencia;
    @SerializedName("idTurno")
    @Expose
    private Integer idTurno;
    @SerializedName("filtroOp")
    @Expose
    private Integer filtroOp;
    @SerializedName("cdGt")
    @Expose
    private String cdGt;
    @SerializedName("listaFiltroPostos")
    @Expose
    private List<Object> listaFiltroPostos = null;
    @SerializedName("listaFiltroPtCp")
    @Expose
    private List<Object> listaFiltroPtCp = null;

    public String getDtReferencia() {
        return dtReferencia;
    }

    public void setDtReferencia(String dtReferencia) {
        this.dtReferencia = dtReferencia;
    }

    public Integer getIdTurno() {
        return idTurno;
    }

    public void setIdTurno(Integer idTurno) {
        this.idTurno = idTurno;
    }

    public Integer getFiltroOp() {
        return filtroOp;
    }

    public void setFiltroOp(Integer filtroOp) {
        this.filtroOp = filtroOp;
    }

    public String getCdGt() {
        return cdGt;
    }

    public void setCdGt(String cdGt) {
        this.cdGt = cdGt;
    }

    public List<Object> getListaFiltroPostos() {
        return listaFiltroPostos;
    }

    public void setListaFiltroPostos(List<Object> listaFiltroPostos) {
        this.listaFiltroPostos = listaFiltroPostos;
    }

    public List<Object> getListaFiltroPtCp() {
        return listaFiltroPtCp;
    }

    public void setListaFiltroPtCp(List<Object> listaFiltroPtCp) {
        this.listaFiltroPtCp = listaFiltroPtCp;
    }

}