package com.example.lajos.shopfloor.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.lajos.shopfloor.data.WorkGroup;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class WorkgroupSpinAdapter extends ArrayAdapter<WorkGroup>{

    private Context context;
    private ArrayList<WorkGroup> mGroups;

    public WorkgroupSpinAdapter(@NonNull Context context, int textViewResourceId,
                                @NonNull ArrayList<WorkGroup> mGroups) {
        super(context, textViewResourceId, mGroups);
        this.mGroups = mGroups;
    }

    @Override
    public int getCount() {
        return mGroups.size();
    }

    @Nullable
    @Override
    public WorkGroup getItem(int position) {
        return mGroups.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        super.getView(position, convertView, parent);

        TextView workGroupName = (TextView) super.getView(position, convertView, parent);
        workGroupName.setTextColor(Color.BLACK);
        return workGroupName;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        super.getDropDownView(position, convertView, parent);

        TextView workgroupName = (TextView) super.getDropDownView(position, convertView, parent);
        workgroupName.setTextColor(Color.BLACK);
        return workgroupName;
    }
}
