package com.example.lajos.shopfloor.network.pojo;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AnalyzesResponse {

    @SerializedName("perdasMateriaPrima")
    @Expose
    private List<LoseRawMaterialPojo> perdasMateriaPrima = null;
    @SerializedName("perdasFerramenta")
    @Expose
    private List<LoseToolPojo> perdasFerramenta = null;
    @SerializedName("legendasMateriaPrima")
    @Expose
    private List<LegendRawMaterialPojo> legendasMateriaPrima = null;
    @SerializedName("legendasFerramenta")
    @Expose
    private List<LegendToolPojo> legendasFerramenta = null;
    @SerializedName("graficoDropRate")
    @Expose
    private List<Object> graficoDropRate = null;
    @SerializedName("graficoCustoPorPerda")
    @Expose
    private List<Object> graficoCustoPorPerda = null;

    public List<LoseRawMaterialPojo> getPerdasMateriaPrima() {
        return perdasMateriaPrima;
    }

    public void setPerdasMateriaPrima(List<LoseRawMaterialPojo> perdasMateriaPrima) {
        this.perdasMateriaPrima = perdasMateriaPrima;
    }

    public List<LoseToolPojo> getPerdasFerramenta() {
        return perdasFerramenta;
    }

    public void setPerdasFerramenta(List<LoseToolPojo> perdasFerramenta) {
        this.perdasFerramenta = perdasFerramenta;
    }

    public List<LegendRawMaterialPojo> getLegendasMateriaPrima() {
        return legendasMateriaPrima;
    }

    public void setLegendasMateriaPrima(List<LegendRawMaterialPojo> legendasMateriaPrima) {
        this.legendasMateriaPrima = legendasMateriaPrima;
    }

    public List<LegendToolPojo> getLegendasFerramenta() {
        return legendasFerramenta;
    }

    public void setLegendasFerramenta(List<LegendToolPojo> legendasFerramenta) {
        this.legendasFerramenta = legendasFerramenta;
    }

    public List<Object> getGraficoDropRate() {
        return graficoDropRate;
    }

    public void setGraficoDropRate(List<Object> graficoDropRate) {
        this.graficoDropRate = graficoDropRate;
    }

    public List<Object> getGraficoCustoPorPerda() {
        return graficoCustoPorPerda;
    }

    public void setGraficoCustoPorPerda(List<Object> graficoCustoPorPerda) {
        this.graficoCustoPorPerda = graficoCustoPorPerda;
    }

}