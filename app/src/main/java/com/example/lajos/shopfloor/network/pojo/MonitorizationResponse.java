package com.example.lajos.shopfloor.network.pojo;

import java.util.List;

import com.example.lajos.shopfloor.data.WorkGroup;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MonitorizationResponse {

    @SerializedName("filtro")
    @Expose
    private FilterPojo filtro;
    @SerializedName("filtroAnaliseGargalo")
    @Expose
    private FilterBottleneckAnalyzesPojo filtroAnaliseGargalo;
    @SerializedName("gt")
    @Expose
    private WorkGroup gt;
    @SerializedName("gts")
    @Expose
    private List<WorkGroupMonitorizationPojo> gts = null;
    @SerializedName("pts")
    @Expose
    private List<Object> pts = null;

    public FilterPojo getFiltro() {
        return filtro;
    }

    public void setFiltro(FilterPojo filtro) {
        this.filtro = filtro;
    }

    public FilterBottleneckAnalyzesPojo getFiltroAnaliseGargalo() {
        return filtroAnaliseGargalo;
    }

    public void setFiltroAnaliseGargalo(FilterBottleneckAnalyzesPojo filtroAnaliseGargalo) {
        this.filtroAnaliseGargalo = filtroAnaliseGargalo;
    }

    public WorkGroup getGt() {
        return gt;
    }

    public void setGt(WorkGroup gt) {
        this.gt = gt;
    }

    public List<WorkGroupMonitorizationPojo> getGts() {
        return gts;
    }

    public void setGts(List<WorkGroupMonitorizationPojo> gts) {
        this.gts = gts;
    }

    public List<Object> getPts() {
        return pts;
    }

    public void setPts(List<Object> pts) {
        this.pts = pts;
    }

}
