package com.example.lajos.shopfloor.activity;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.speech.RecognizerIntent;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.lajos.shopfloor.R;
import com.example.lajos.shopfloor.adapter.WorkGroupMonitorizationAdapter;
import com.example.lajos.shopfloor.network.calls.WmApiCalls;
import com.example.lajos.shopfloor.network.pojo.ActualTurnResponse;
import com.example.lajos.shopfloor.network.pojo.FilterDetailPojo;
import com.example.lajos.shopfloor.network.pojo.MonitorizationResponse;
import com.example.lajos.shopfloor.network.pojo.WorkGroupMonitorizationPojo;
import com.example.lajos.shopfloor.network.service.WmService;
import com.example.lajos.shopfloor.util.UserSession;
import com.leo.simplearcloader.ArcConfiguration;
import com.leo.simplearcloader.SimpleArcDialog;

import java.util.ArrayList;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MonitorizationActivity extends AppCompatActivity {

    UserSession session;
    Context context;
    String workgroup_code;
    String workgroup_name;
    String token;
    ArrayList<WorkGroupMonitorizationPojo> wgs = new ArrayList<>();
    RecyclerView wgsRecycler;
    WorkGroupMonitorizationAdapter wgsAdapter;

    static int teste = 0;


    protected static TextToSpeech ttobj;
    protected static final int RESULT_SPEECH = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_monitorization);
        context = getApplicationContext();
        session = new UserSession(context);
        workgroup_code = getIntent().getStringExtra("workgroup_code");
        workgroup_name = getIntent().getStringExtra("workgroup_name");
        token = session.getToken();
        WmApiCalls calls = new WmApiCalls(context, MonitorizationActivity.this);


        Toolbar toolbar = findViewById(R.id.toolbar);
        Button updateButton = findViewById(R.id.update_button);
        Button turnAnalyzesButton = findViewById(R.id.turn_analyzes_button);
        Button legendButton = findViewById(R.id.legend_button);
        ImageButton voiceButton = findViewById(R.id.voice_button);
        wgsRecycler = findViewById(R.id.monitorization_workgroup_recycler);
        GridLayoutManager layoutManager = new GridLayoutManager(this, 3);

        wgsAdapter = new WorkGroupMonitorizationAdapter(wgs);
        wgsRecycler.setLayoutManager(layoutManager);
        wgsRecycler.setAdapter(wgsAdapter);

        toolbar.setTitle("WM - Monitorização : " + workgroup_name);
        setSupportActionBar(toolbar);

        calls.getActualTurn(workgroup_code,token,wgs,wgsAdapter);

        ttobj = new TextToSpeech(getApplicationContext(),
                new TextToSpeech.OnInitListener() {
                    @Override
                    public void onInit(int status) {
                        if (status != TextToSpeech.ERROR) {
                            ttobj.setLanguage(new Locale("pt", "br"));
                        }
                    }
                });

        voiceButton.setOnClickListener(new View.OnClickListener() {
            //
            @Override
            public void onClick(View v) {
                if (teste == 0) {
                    teste = 1;
                    //ttobj.speak("Olá, Você gostaria de uma análise de balanceamento ou uma análise por perda de componente?", TextToSpeech.QUEUE_FLUSH, null);
                }
                readMicrofone();
            }
        });

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                falar("Tela de linhas cadastradas no grupo de trabalho aberta");  //speak after 1000ms
            }
        }, 500);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.toolbar_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.logout:
                //session.logout();
                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(intent);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case RESULT_SPEECH: {
                if (resultCode == RESULT_OK && null != data) {
                    ArrayList<String> text = data
                            .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    String texto = text.get(0);
                    falar(texto);
                }
                break;
            }
        }
    }

    public void falar(String texto) {
        if (texto != null && texto.isEmpty() == false) {

            if (texto.contains("perda")) {

                // String nova = texto.replaceAll(" \\S*$", texto.substring(texto.lastIndexOf(" ")+1).replace(""," "));
                //ttobj.speak(texto.substring(0, texto.lastIndexOf(" ")) + texto.substring(texto.lastIndexOf(" ") + 1).replace("", " "), TextToSpeech.QUEUE_FLUSH, null);
                try {
                    Thread.sleep(5);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                // ttobj.speak("A principal perda de componente está na Linha 09. Você gostaria de uma análise de balanceamento ou uma análise por perda de componente? ", TextToSpeech.QUEUE_FLUSH, null);
                ttobj.speak("O componente xyz123 na máquina cp3,apresenta problema no feeder 5n343 na posição 72. É necessário Trocá-lo imediatamente.", TextToSpeech.QUEUE_FLUSH, null);
            } else if (texto.contains("pior")) {
                try {
                    Thread.sleep(5);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                ttobj.speak("A linha com pior desempenho OEE é a Linha 05 . Você gostaria de uma análise de balanceamento ou uma análise por perda de componente? ", TextToSpeech.QUEUE_FLUSH, null);
            }


            else if (texto.contains("abrir")) {
                try {
                    Thread.sleep(5);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                ttobj.speak("Abrindo o grupo de trabalho de testes da MAP", TextToSpeech.QUEUE_FLUSH, null);
                Intent intent = new Intent(this, MonitorizationActivity.class);
                intent.putExtra("workgroup_code", "FABMAP");
                intent.putExtra("workgroup_name", "Grupo de testes da MAP");
                this.startActivity(intent);
            }



//            else if (texto.contains("causa")) {
//
//                // String nova = texto.replaceAll(" \\S*$", texto.substring(texto.lastIndexOf(" ")+1).replace(""," "));
//                //ttobj.speak(texto.substring(0, texto.lastIndexOf(" ")) + texto.substring(texto.lastIndexOf(" ") + 1).replace("", " "), TextToSpeech.QUEUE_FLUSH, null);
//
//
//                try {
//                    Thread.sleep(5);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//
//                ttobj.speak("A principal causa é o problema no Feeder", TextToSpeech.QUEUE_FLUSH, null);
//            }
            else {
                try {
                    ttobj.speak(texto, TextToSpeech.QUEUE_FLUSH, null);
                    Thread.sleep(3);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }


            }
            //ttobj.speak(texto, TextToSpeech.QUEUE_FLUSH, null);
        }
    }

    public void readMicrofone() {
        Intent intent = new Intent(
                RecognizerIntent.ACTION_RECOGNIZE_SPEECH);

        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, "en-US");

        try {
            startActivityForResult(intent, RESULT_SPEECH);

        } catch (ActivityNotFoundException a) {
            Toast t = Toast.makeText(this,
                    "Ops! Your device doesn't support Speech to Text",
                    Toast.LENGTH_SHORT);
            t.show();
        }
    }
}
