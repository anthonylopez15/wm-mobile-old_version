package com.example.lajos.shopfloor.network.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AnalyzesFilterPostPojo {

    @SerializedName("dtReferencia")
    @Expose
    private String dtReferencia;
    @SerializedName("idTurno")
    @Expose
    private Integer idTurno;
    @SerializedName("filtroOp")
    @Expose
    private Integer filtroOp;
    @SerializedName("cdGt")
    @Expose
    private String cdGt;
    @SerializedName("turnoAtual")
    @Expose
    private Boolean turnoAtual;
    @SerializedName("tpId")
    @Expose
    private Integer tpId;

    public AnalyzesFilterPostPojo(String dtReferencia, Integer idTurno, Integer filtroOp, String cdGt, Boolean turnoAtual, Integer tpId) {
        this.dtReferencia = dtReferencia;
        this.idTurno = idTurno;
        this.filtroOp = filtroOp;
        this.cdGt = cdGt;
        this.turnoAtual = turnoAtual;
        this.tpId = tpId;
    }

    public String getDtReferencia() {
        return dtReferencia;
    }

    public void setDtReferencia(String dtReferencia) {
        this.dtReferencia = dtReferencia;
    }

    public Integer getIdTurno() {
        return idTurno;
    }

    public void setIdTurno(Integer idTurno) {
        this.idTurno = idTurno;
    }

    public Integer getFiltroOp() {
        return filtroOp;
    }

    public void setFiltroOp(Integer filtroOp) {
        this.filtroOp = filtroOp;
    }

    public String getCdGt() {
        return cdGt;
    }

    public void setCdGt(String cdGt) {
        this.cdGt = cdGt;
    }

    public Boolean getTurnoAtual() {
        return turnoAtual;
    }

    public void setTurnoAtual(Boolean turnoAtual) {
        this.turnoAtual = turnoAtual;
    }

    public Integer getTpId() {
        return tpId;
    }

    public void setTpId(Integer tpId) {
        this.tpId = tpId;
    }

}