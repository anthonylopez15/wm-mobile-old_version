package com.example.lajos.shopfloor.data;

public class Report {

    private String date;
    private String description;

    public Report(String date, String description) {
        this.date = date;
        this.description = description;
    }

    public String getDate() {
        return date;
    }

    public String getDescription() {
        return description;
    }
}