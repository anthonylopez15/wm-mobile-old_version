package com.example.lajos.shopfloor.data;

public class WorkGroup {

    String workGroupName;
    String workGroupCode;

    public WorkGroup(String workGroupName, String workGroupCode) {
        this.workGroupName = workGroupName;
        this.workGroupCode = workGroupCode;
    }

    public String getWorkGroupName() {
        return workGroupName;
    }

    public void setWorkGroupName(String workGroupName) {
        this.workGroupName = workGroupName;
    }

    public String getWorkGroupCode() {
        return workGroupCode;
    }

    public void setWorkGroupCode(String workGroupCode) {
        this.workGroupCode = workGroupCode;
    }

    public String toString(){ return  this.workGroupName;}
}
