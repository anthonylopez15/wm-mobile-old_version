package com.example.lajos.shopfloor.network.pojo;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Filtro__ {

    @SerializedName("filtro")
    @Expose
    private Filtro___ filtro;
    @SerializedName("listaMpBruta")
    @Expose
    private List<Object> listaMpBruta = null;

    public Filtro___ getFiltro() {
        return filtro;
    }

    public void setFiltro(Filtro___ filtro) {
        this.filtro = filtro;
    }

    public List<Object> getListaMpBruta() {
        return listaMpBruta;
    }

    public void setListaMpBruta(List<Object> listaMpBruta) {
        this.listaMpBruta = listaMpBruta;
    }

}