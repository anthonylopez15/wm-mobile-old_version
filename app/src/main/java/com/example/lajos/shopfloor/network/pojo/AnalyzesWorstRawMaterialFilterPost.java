package com.example.lajos.shopfloor.network.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AnalyzesWorstRawMaterialFilterPost {

    @SerializedName("filtro")
    @Expose
    private AnalyzesWorstRawMaterialPostPojo filtro;

    public AnalyzesWorstRawMaterialFilterPost(AnalyzesWorstRawMaterialPostPojo filtro) {
        this.filtro = filtro;
    }

    public AnalyzesWorstRawMaterialPostPojo getFiltro() {
        return filtro;
    }

    public void setFiltro(AnalyzesWorstRawMaterialPostPojo filtro) {
        this.filtro = filtro;
    }

}