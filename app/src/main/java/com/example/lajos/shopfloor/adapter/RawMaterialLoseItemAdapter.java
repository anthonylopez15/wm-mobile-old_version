package com.example.lajos.shopfloor.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.lajos.shopfloor.R;
import com.example.lajos.shopfloor.network.pojo.LoseRawMaterialPojo;

import java.util.ArrayList;

public class RawMaterialLoseItemAdapter extends RecyclerView.Adapter<RawMaterialLoseItemAdapter.RawMaterialLoseItemHolder> {

    private final ArrayList<LoseRawMaterialPojo> rawLoses;

    public RawMaterialLoseItemAdapter(ArrayList<LoseRawMaterialPojo> rawLoses) {
        this.rawLoses = rawLoses;
    }

    @NonNull
    @Override
    public RawMaterialLoseItemHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new RawMaterialLoseItemHolder(LayoutInflater.from(parent.getContext())
        .inflate(R.layout.raw_material_lose_card, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RawMaterialLoseItemHolder holder, int position) {
        final LoseRawMaterialPojo rawLose = rawLoses.get(position);
        holder.materialName.setText(rawLose.getCdProduto());
        holder.rawLoseValue.setText(rawLose.getQuantidadePerdida());
        holder.rawLosePercentage.setText(rawLose.getPorcentagemPerda());
    }

    @Override
    public int getItemCount() {
        return rawLoses.size();
    }

    public class RawMaterialLoseItemHolder extends RecyclerView.ViewHolder{

        CardView card;
        TextView materialName;
        TextView rawLoseValue;
        TextView rawLosePercentage;

        public RawMaterialLoseItemHolder(View itemView) {
            super(itemView);
            card = itemView.findViewById(R.id.card_view);
            materialName = itemView.findViewById(R.id.material_name_text);
            rawLoseValue = itemView.findViewById(R.id.raw_lose_value);
            rawLosePercentage = itemView.findViewById(R.id.raw_lose_percentage);
        }
    }
}
