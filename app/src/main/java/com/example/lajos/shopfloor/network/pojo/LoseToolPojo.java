package com.example.lajos.shopfloor.network.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoseToolPojo {

    @SerializedName("cdFerramenta")
    @Expose
    private String cdFerramenta;
    @SerializedName("corOcorrencia")
    @Expose
    private String corOcorrencia;
    @SerializedName("quantidadePerdida")
    @Expose
    private String quantidadePerdida;
    @SerializedName("filtro")
    @Expose
    private Filtro__ filtro;

    public String getCdFerramenta() {
        return cdFerramenta;
    }

    public void setCdFerramenta(String cdFerramenta) {
        this.cdFerramenta = cdFerramenta;
    }

    public String getCorOcorrencia() {
        return corOcorrencia;
    }

    public void setCorOcorrencia(String corOcorrencia) {
        this.corOcorrencia = corOcorrencia;
    }

    public String getQuantidadePerdida() {
        return quantidadePerdida;
    }

    public void setQuantidadePerdida(String quantidadePerdida) {
        this.quantidadePerdida = quantidadePerdida;
    }

    public Filtro__ getFiltro() {
        return filtro;
    }

    public void setFiltro(Filtro__ filtro) {
        this.filtro = filtro;
    }

}