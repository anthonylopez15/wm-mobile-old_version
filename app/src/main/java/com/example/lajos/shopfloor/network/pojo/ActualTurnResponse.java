package com.example.lajos.shopfloor.network.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ActualTurnResponse {

    @SerializedName("idTurno")
    @Expose
    private Integer idTurno;
    @SerializedName("cdTurno")
    @Expose
    private String cdTurno;
    @SerializedName("dsTurno")
    @Expose
    private String dsTurno;
    @SerializedName("dtReferencia")
    @Expose
    private String dtReferencia;

    public Integer getIdTurno() {
        return idTurno;
    }

    public void setIdTurno(Integer idTurno) {
        this.idTurno = idTurno;
    }

    public String getCdTurno() {
        return cdTurno;
    }

    public void setCdTurno(String cdTurno) {
        this.cdTurno = cdTurno;
    }

    public String getDsTurno() {
        return dsTurno;
    }

    public void setDsTurno(String dsTurno) {
        this.dsTurno = dsTurno;
    }

    public String getDtReferencia() {
        return dtReferencia;
    }

    public void setDtReferencia(String dtReferencia) {
        this.dtReferencia = dtReferencia;
    }

}