package com.example.lajos.shopfloor.network.pojo;

import android.support.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WorkGroupMonitorizationPojo {

    @SerializedName("cdGt")
    @Expose
    private String cdGt;
    @SerializedName("dsGt")
    @Expose
    private String dsGt;
    @SerializedName("dsGtView")
    @Expose
    private String dsGtView;
    @SerializedName("caminhoIcone")
    @Expose
    private String caminhoIcone;
    @SerializedName("corFundo")
    @Expose
    private String corFundo;
    @SerializedName("indiceOEE")
    @Expose
    private String indiceOEE;
    @SerializedName("indiceOEEMeta")
    @Expose
    private String indiceOEEMeta;
    @SerializedName("quantidadePostos")
    @Expose
    private Integer quantidadePostos;
    @SerializedName("filtroDetalhe")
    @Expose
    private FilterDetailPojo filtroDetalhe;

    public String getCdGt() {
        return cdGt;
    }

    public void setCdGt(String cdGt) {
        this.cdGt = cdGt;
    }

    public String getDsGt() {
        return dsGt;
    }

    public void setDsGt(String dsGt) {
        this.dsGt = dsGt;
    }

    public String getDsGtView() {
        return dsGtView;
    }

    public void setDsGtView(String dsGtView) {
        this.dsGtView = dsGtView;
    }

    public String getCaminhoIcone() {
        return caminhoIcone;
    }

    public void setCaminhoIcone(String caminhoIcone) {
        this.caminhoIcone = caminhoIcone;
    }

    public String getCorFundo() {
        return corFundo;
    }

    public void setCorFundo(String corFundo) {
        this.corFundo = corFundo;
    }

    public String getIndiceOEE() {
        return indiceOEE;
    }

    public void setIndiceOEE(String indiceOEE) {
        this.indiceOEE = indiceOEE;
    }

    public String getIndiceOEEMeta() {
        return indiceOEEMeta;
    }

    public void setIndiceOEEMeta(String indiceOEEMeta) {
        this.indiceOEEMeta = indiceOEEMeta;
    }

    public Integer getQuantidadePostos() {
        return quantidadePostos;
    }

    public void setQuantidadePostos(Integer quantidadePostos) {
        this.quantidadePostos = quantidadePostos;
    }

    public FilterDetailPojo getFiltroDetalhe() {
        return filtroDetalhe;
    }

    public void setFiltroDetalhe(FilterDetailPojo filtroDetalhe) {
        this.filtroDetalhe = filtroDetalhe;
    }

}