package com.example.lajos.shopfloor.network.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WorkGroupPojo {

    @SerializedName("idGt")
    @Expose
    private String idGt;
    @SerializedName("cdGt")
    @Expose
    private String cdGt;
    @SerializedName("dsGt")
    @Expose
    private String dsGt;
    @SerializedName("dsView")
    @Expose
    private String dsView;

    public String getIdGt() {
        return idGt;
    }

    public void setIdGt(String idGt) {
        this.idGt = idGt;
    }

    public String getCdGt() {
        return cdGt;
    }

    public void setCdGt(String cdGt) {
        this.cdGt = cdGt;
    }

    public String getDsGt() {
        return dsGt;
    }

    public void setDsGt(String dsGt) {
        this.dsGt = dsGt;
    }

    public String getDsView() {
        return dsView;
    }

    public void setDsView(String dsView) {
        this.dsView = dsView;
    }

}