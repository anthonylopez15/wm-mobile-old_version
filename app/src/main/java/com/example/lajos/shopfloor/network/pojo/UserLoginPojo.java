package com.example.lajos.shopfloor.network.pojo;

public class UserLoginPojo {
    String login;
    String senha;

    public UserLoginPojo(String login, String password) {
        this.login = login;
        this.senha = password;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return senha;
    }

    public void setPassword(String password) {
        this.senha = password;
    }
}
