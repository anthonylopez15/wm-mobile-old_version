package com.example.lajos.shopfloor.network.pojo;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FiltroPojo {

    @SerializedName("filtro")
    @Expose
    private Filtro_ filtro;
    @SerializedName("listaMpBruta")
    @Expose
    private List<Object> listaMpBruta = null;

    public Filtro_ getFiltro() {
        return filtro;
    }

    public void setFiltro(Filtro_ filtro) {
        this.filtro = filtro;
    }

    public List<Object> getListaMpBruta() {
        return listaMpBruta;
    }

    public void setListaMpBruta(List<Object> listaMpBruta) {
        this.listaMpBruta = listaMpBruta;
    }

}