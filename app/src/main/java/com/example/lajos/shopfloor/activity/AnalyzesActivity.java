package com.example.lajos.shopfloor.activity;

import android.content.Context;
import android.content.Intent;
import android.media.MediaCas;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.lajos.shopfloor.R;
import com.example.lajos.shopfloor.adapter.RawMaterialLoseItemAdapter;
import com.example.lajos.shopfloor.network.calls.WmApiCalls;
import com.example.lajos.shopfloor.network.pojo.AnalyzesFilterPost;
import com.example.lajos.shopfloor.network.pojo.AnalyzesFilterPostPojo;
import com.example.lajos.shopfloor.network.pojo.AnalyzesResponse;
import com.example.lajos.shopfloor.network.pojo.LoseRawMaterialPojo;
import com.example.lajos.shopfloor.network.service.WmService;
import com.example.lajos.shopfloor.util.UserSession;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.HorizontalBarChart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.DefaultValueFormatter;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class AnalyzesActivity extends AppCompatActivity {

    UserSession session;
    String token;
    AnalyzesFilterPostPojo analyzesFilterPojo;
    AnalyzesFilterPost analyzesFilterPost;
    RecyclerView rawLoseRecycler;
    GridLayoutManager layoutManager;
    RawMaterialLoseItemAdapter rawLoseAdapter;
    ArrayList<LoseRawMaterialPojo> rawLoses = new ArrayList<>();


    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_analyzes);
        Context context = getApplicationContext();
        Intent intent = getIntent();
        session = new UserSession(getApplicationContext());
        token = session.getToken();
        WmApiCalls calls = new WmApiCalls(context, AnalyzesActivity.this);

        rawLoseRecycler = findViewById(R.id.raw_lose_recycler);
        layoutManager = new GridLayoutManager(this, 3){
            @Override
            public boolean canScrollVertically() {
                return false;
            }
        };

        rawLoseAdapter = new RawMaterialLoseItemAdapter(rawLoses);
        rawLoseRecycler.setLayoutManager(layoutManager);
        rawLoseRecycler.setAdapter(rawLoseAdapter);

        analyzesFilterPojo = new AnalyzesFilterPostPojo(
                "04/07/2018",
                intent.getIntExtra("id_turno", 0),
                intent.getIntExtra("filtro_op", 0),
                intent.getStringExtra("cd_gt"),
                intent.getBooleanExtra("turno_atual", false),
                1);
        analyzesFilterPost = new AnalyzesFilterPost(analyzesFilterPojo);

        calls.getAnalyzes(analyzesFilterPost, token, rawLoses, rawLoseAdapter);
    }

}
