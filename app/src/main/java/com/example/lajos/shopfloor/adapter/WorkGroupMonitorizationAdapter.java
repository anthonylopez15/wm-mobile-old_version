package com.example.lajos.shopfloor.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.lajos.shopfloor.R;
import com.example.lajos.shopfloor.activity.AnalyzesActivity;
import com.example.lajos.shopfloor.network.pojo.FilterDetailPojo;
import com.example.lajos.shopfloor.network.pojo.WorkGroupMonitorizationPojo;

import java.net.ConnectException;
import java.util.ArrayList;


public class WorkGroupMonitorizationAdapter extends RecyclerView.Adapter<WorkGroupMonitorizationAdapter.LineHolder> {

    private final ArrayList<WorkGroupMonitorizationPojo> wgs;

    public WorkGroupMonitorizationAdapter(ArrayList<WorkGroupMonitorizationPojo> lines) {
        this.wgs = lines;
    }

    @NonNull
    @Override
    public LineHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new LineHolder(LayoutInflater.from(parent.getContext())
        .inflate(R.layout.workgroup_monitorization_line_card, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull LineHolder holder, int position) {
        final Context context = holder.itemView.getContext();
        WorkGroupMonitorizationPojo wg = wgs.get(position);
        final FilterDetailPojo fwg = wg.getFiltroDetalhe();
        holder.lineName.setText(wg.getDsGt());
        holder.oeeValue.setText(wg.getIndiceOEE());
        holder.metaValue.setText(wg.getIndiceOEEMeta());
        holder.qtdPostos.setText(String.valueOf(wg.getQuantidadePostos()));

        holder.card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, AnalyzesActivity.class);
                intent.putExtra("dt_referencia", fwg.getDtReferencia());
                intent.putExtra("id_turno", fwg.getIdTurno());
                intent.putExtra("filtro_op", fwg.getFiltroOp());
                intent.putExtra("cd_gt", fwg.getCdGt());
                intent.putExtra("turno_atual", fwg.getTurnoAtual());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return wgs.size();
    }

    public class LineHolder extends RecyclerView.ViewHolder{

        CardView card;
        TextView lineName;
        TextView oeeValue;
        TextView metaValue;
        TextView qtdPostos;

        public LineHolder(View itemView) {
            super(itemView);
            card = itemView.findViewById(R.id.card_view);
            lineName = itemView.findViewById(R.id.line_name);
            oeeValue = itemView.findViewById(R.id.oee_value);
            metaValue = itemView.findViewById(R.id.meta_value);
            qtdPostos = itemView.findViewById(R.id.postos_value);
        }
    }
}
