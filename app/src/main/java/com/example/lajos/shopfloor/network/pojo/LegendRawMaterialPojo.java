package com.example.lajos.shopfloor.network.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LegendRawMaterialPojo {

    @SerializedName("dataHoraInicio")
    @Expose
    private String dataHoraInicio;
    @SerializedName("dataHoraFim")
    @Expose
    private String dataHoraFim;
    @SerializedName("legendaCor")
    @Expose
    private String legendaCor;

    public String getDataHoraInicio() {
        return dataHoraInicio;
    }

    public void setDataHoraInicio(String dataHoraInicio) {
        this.dataHoraInicio = dataHoraInicio;
    }

    public String getDataHoraFim() {
        return dataHoraFim;
    }

    public void setDataHoraFim(String dataHoraFim) {
        this.dataHoraFim = dataHoraFim;
    }

    public String getLegendaCor() {
        return legendaCor;
    }

    public void setLegendaCor(String legendaCor) {
        this.legendaCor = legendaCor;
    }

}