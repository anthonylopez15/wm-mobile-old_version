package com.example.lajos.shopfloor.network.pojo;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AnalyzesWorstRawMaterialPostPojo {

    @SerializedName("idConsolid")
    @Expose
    private Integer idConsolid;
    @SerializedName("filtroOp")
    @Expose
    private Integer filtroOp;
    @SerializedName("dtReferencia")
    @Expose
    private String dtReferencia;
    @SerializedName("idTurno")
    @Expose
    private Integer idTurno;
    @SerializedName("cdPosto")
    @Expose
    private String cdPosto;
    @SerializedName("tpId")
    @Expose
    private Integer tpId;
    @SerializedName("idProduto")
    @Expose
    private Integer idProduto;
    @SerializedName("tipoPosto")
    @Expose
    private Integer tipoPosto;
    @SerializedName("idRap")
    @Expose
    private Integer idRap;

    public AnalyzesWorstRawMaterialPostPojo(Integer idConsolid, Integer filtroOp, String dtReferencia, Integer idTurno, String cdPosto, Integer tpId, Integer idProduto, Integer tipoPosto, Integer idRap) {
        this.idConsolid = idConsolid;
        this.filtroOp = filtroOp;
        this.dtReferencia = dtReferencia;
        this.idTurno = idTurno;
        this.cdPosto = cdPosto;
        this.tpId = tpId;
        this.idProduto = idProduto;
        this.tipoPosto = tipoPosto;
        this.idRap = idRap;
    }

    public Integer getIdConsolid() {
        return idConsolid;
    }

    public void setIdConsolid(Integer idConsolid) {
        this.idConsolid = idConsolid;
    }

    public Integer getFiltroOp() {
        return filtroOp;
    }

    public void setFiltroOp(Integer filtroOp) {
        this.filtroOp = filtroOp;
    }

    public String getDtReferencia() {
        return dtReferencia;
    }

    public void setDtReferencia(String dtReferencia) {
        this.dtReferencia = dtReferencia;
    }

    public Integer getIdTurno() {
        return idTurno;
    }

    public void setIdTurno(Integer idTurno) {
        this.idTurno = idTurno;
    }

    public String getCdPosto() {
        return cdPosto;
    }

    public void setCdPosto(String cdPosto) {
        this.cdPosto = cdPosto;
    }

    public Integer getTpId() {
        return tpId;
    }

    public void setTpId(Integer tpId) {
        this.tpId = tpId;
    }

    public Integer getIdProduto() {
        return idProduto;
    }

    public void setIdProduto(Integer idProduto) {
        this.idProduto = idProduto;
    }

    public Integer getTipoPosto() {
        return tipoPosto;
    }

    public void setTipoPosto(Integer tipoPosto) {
        this.tipoPosto = tipoPosto;
    }

    public Integer getIdRap() {
        return idRap;
    }

    public void setIdRap(Integer idRap) {
        this.idRap = idRap;
    }

}
