package com.example.lajos.shopfloor.activity;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.speech.RecognizerIntent;
import android.speech.tts.TextToSpeech;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.telephony.SmsManager;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.lajos.shopfloor.R;
import com.example.lajos.shopfloor.adapter.ReportAdapter;
import com.example.lajos.shopfloor.adapter.WorkGroupAdapter;
import com.example.lajos.shopfloor.adapter.WorkGroupMonitorizationAdapter;
import com.example.lajos.shopfloor.adapter.WorkgroupSpinAdapter;
import com.example.lajos.shopfloor.data.Report;
import com.example.lajos.shopfloor.data.WorkGroup;
import com.example.lajos.shopfloor.network.calls.WmApiCalls;
import com.example.lajos.shopfloor.network.pojo.AnalyzesFilterPost;
import com.example.lajos.shopfloor.network.pojo.AnalyzesFilterPostPojo;
import com.example.lajos.shopfloor.network.pojo.WorkGroupMonitorizationPojo;
import com.example.lajos.shopfloor.util.UserSession;

import org.angmarch.views.NiceSpinner;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Calendar;

public class MasterActivity extends AppCompatActivity {

    WmApiCalls calls;
    UserSession session;
    RecyclerView wgsRecycler;
    RecyclerView reportRecycler;
    NiceSpinner workgroupSpinner;
    WorkGroupMonitorizationAdapter wgsAdapter;
    ReportAdapter reportAdapter;
    String workgroup_code;
    String token;
    ArrayList<Report> reports = new ArrayList<>();
    ArrayList<WorkGroup> spinnerGroups = new ArrayList<>();
    ArrayList<WorkGroupMonitorizationPojo> selectedGroupLines = new ArrayList<>();
    ArrayList<WorkGroupMonitorizationPojo> selectedGroupLinesPerformanceOrder = new ArrayList<>();
    WorkGroupMonitorizationPojo worstGroupLinePerformance = new WorkGroupMonitorizationPojo();
    static int teste = 0;
    protected static TextToSpeech ttobj;
    protected static final int RESULT_SPEECH = 1;
    AnalyzesFilterPostPojo analyzesFilterPojo;
    AnalyzesFilterPost analyzesFilterPost;
    ImageButton voiceButton;
    Calendar currentTime;
    SimpleDateFormat df;
    ArrayList<String> reportDescriptions = new ArrayList<>();
    ArrayList<String> reportDates = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_master);
        final Context context = getApplicationContext();
        session = new UserSession(context);
        token = session.getToken();
        currentTime = Calendar.getInstance();
        df = new SimpleDateFormat("HH:mm:ss");

        workgroupSpinner = findViewById(R.id.workgroup_spinner);
        wgsRecycler = findViewById(R.id.monitorization_workgroup_recycler);
        reportRecycler = findViewById(R.id.report_recycler);
        Toolbar toolbar = findViewById(R.id.toolbar);
        voiceButton = findViewById(R.id.voice_button);

        setSupportActionBar(toolbar);

        ttobj = new TextToSpeech(getApplicationContext(),
                new TextToSpeech.OnInitListener() {
                    @Override
                    public void onInit(int status) {
                        if (status != TextToSpeech.ERROR) {
                            ttobj.setLanguage(new Locale("pt", "br"));
                        }
                    }
                });

        GridLayoutManager layoutManager = new GridLayoutManager(this, 2);
        wgsAdapter = new WorkGroupMonitorizationAdapter(selectedGroupLines);
        wgsRecycler.setLayoutManager(layoutManager);
        wgsRecycler.setAdapter(wgsAdapter);

        LinearLayoutManager layoutManager2 = new LinearLayoutManager(this);
        reportAdapter = new ReportAdapter(reports);
        reportRecycler.setLayoutManager(layoutManager2);
        reportRecycler.setAdapter(reportAdapter);

        calls = new WmApiCalls(context, MasterActivity.this);
        calls.loadWorkGroupsSpinner(spinnerGroups, workgroupSpinner, token, selectedGroupLines,
                selectedGroupLinesPerformanceOrder, worstGroupLinePerformance, wgsAdapter);

        workgroupSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                workgroup_code = spinnerGroups.get(i).getWorkGroupCode();
                selectedGroupLines.clear();
                selectedGroupLinesPerformanceOrder.clear();
                wgsAdapter.notifyDataSetChanged();
                calls.getActualTurnSpinner(workgroup_code, token,
                        selectedGroupLines,
                        selectedGroupLinesPerformanceOrder,
                        worstGroupLinePerformance,
                        wgsAdapter);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        voiceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                worstGroupLinePerformance = selectedGroupLinesPerformanceOrder.get(0);
                if (teste == 0) {
                    teste = 1;
                }
                readMicrofone();
            }
        });

        //ttobj.speak("Olá Júnior! Bem vindo de volta. Grupo de trabalho CG está disponível e pronto para receber comandos.", TextToSpeech.QUEUE_FLUSH, null);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.logout:
                session.logout();
                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(intent);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.toolbar_menu, menu);
        return true;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode == KeyEvent.KEYCODE_HEADSETHOOK){
            voiceButton.performClick();
            return true;
        }
        return false;
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_SPACE) {
            voiceButton.performClick();
            return true;
        }
        return super.onKeyUp(keyCode, event);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case RESULT_SPEECH: {
                if (resultCode == RESULT_OK && null != data) {
                    ArrayList<String> text = data
                            .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    String texto = text.get(0);
                    falar(texto);
                }
                break;
            }
        }
    }

    @Override
    public void onSaveInstanceState(Bundle savedState) {
        super.onSaveInstanceState(savedState);
        savedState.putStringArrayList("REPORT_DATES", reportDates);
        savedState.putStringArrayList("REPORT_DESCRIPTIONS", reportDescriptions);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        reportDates = savedInstanceState.getStringArrayList("REPORT_DATES");
        reportDescriptions = savedInstanceState.getStringArrayList("REPORT_DESCRIPTIONS");
        for (int i = 0; i< reportDates.size(); i++){
            reports.add(new Report(reportDates.get(i), reportDescriptions.get(i)));
        }
        reportAdapter.notifyDataSetChanged();
    }

    public void falar(String texto) {
        if (texto != null && !texto.isEmpty()) {

            if (texto.contains("bypass")) {

                try {
                    Thread.sleep(5);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                ttobj.speak("O componente xyz123 na máquina cp3,apresenta problema no feeder 5n343 na posição 72. É necessário Trocá-lo imediatamente.", TextToSpeech.QUEUE_FLUSH, null);
            }

            // ======================================================================== //
            // ======================================================================== //
            //        Análise de perda de componente da linha com pior desempenho       //
            // ======================================================================== //
            // ======================================================================== //
            else if (texto.contains("perda")||texto.contains("perdas") &&
                    (texto.contains("componentes")||texto.contains("componente"))) {
                try {
                    Thread.sleep(5);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                analyzesFilterPojo = new AnalyzesFilterPostPojo(
                        "16/07/2018",
                        worstGroupLinePerformance.getFiltroDetalhe().getIdTurno(),
                        0,
                        worstGroupLinePerformance.getFiltroDetalhe().getCdGt(),
                        false,
                        1);
                analyzesFilterPost = new AnalyzesFilterPost(analyzesFilterPojo);
                calls.getMostLossRawMaterial(analyzesFilterPost, token, ttobj, reports, reportAdapter, currentTime, df, reportDates, reportDescriptions);
            }

            // ======================================================================== //
            // ======================================================================== //
            //                   Análise de linha com o pior desempenho                 //
            // ======================================================================== //
            // ======================================================================== //
            else if (texto.contains("falar")
                    && texto.contains("encarregado")){
                try {
                    Thread.sleep(5);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                ttobj.speak("Ligando para o responsável da linha com pior desempenho", TextToSpeech.QUEUE_FLUSH, null);
                try {
                    Thread.sleep(4000);
                    currentTime = Calendar.getInstance();
                    reports.add(0, new Report(df.format(currentTime.getTime()), "Contato de linha com pior desempenho realizado com 982248932.\n"));
                    reportAdapter.notifyDataSetChanged();
                    reportDates.add(0, df.format(currentTime.getTime()));
                    reportDescriptions.add(0, "Contato de linha com pior desempenho realizado com 982248932.2.\n");
                    Intent intent = new Intent(Intent.ACTION_CALL);
                    intent.setData(Uri.parse("tel:"+"982248932"));
                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                        return;
                    }
                    startActivity(intent);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            else if (texto.contains("pior desempenho")) {
                try {
                    Thread.sleep(5);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                ttobj.speak("A linha com pior desempenho é a " + worstGroupLinePerformance.getDsGt() + ".", TextToSpeech.QUEUE_FLUSH, null);
                currentTime = Calendar.getInstance();
                reportDates.add(0, df.format(currentTime.getTime()));
                reportDescriptions.add(0, "A linha com pior desempenho é a " + worstGroupLinePerformance.getDsGt());
                reports.add(0, new Report(df.format(currentTime.getTime()), "A linha com pior desempenho é a " + worstGroupLinePerformance.getDsGt()));
                reportAdapter.notifyDataSetChanged();
            }

            // ======================================================================== //
            // ======================================================================== //
            //             Análise de linhas paradas e envio de relatórios              //
            // ======================================================================== //
            // ======================================================================== //
            else if (texto.contains("mandar")
                    && texto.contains("relatório")){
                try {
                    Thread.sleep(5);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                ttobj.speak("Enviando relatório para o responsável da linha", TextToSpeech.QUEUE_FLUSH, null);
                String report =  "Linha 05 parada - 10 minutos.\n"+
                "Falha no feeder do equipamento xp05.";
                SmsManager.getDefault().sendTextMessage("982248932", null, report, null,null);
                currentTime = Calendar.getInstance();
                reports.add(0, new Report(df.format(currentTime.getTime()), "Relatório de linha parada enviado para 982248932.\n"+report));
                reportAdapter.notifyDataSetChanged();
                reportDates.add(0, df.format(currentTime.getTime()));
                reportDescriptions.add(0, "Relatório de linha parada enviado para 982248932.\n"+report);

            } else if (texto.contains("falar")
                    && texto.contains("supervisor")){
                try {
                    Thread.sleep(5);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                ttobj.speak("Ligando para o responsável da linha", TextToSpeech.QUEUE_FLUSH, null);
                try {
                    Thread.sleep(4000);
                    Intent intent = new Intent(Intent.ACTION_CALL);
                    intent.setData(Uri.parse("tel:"+"982248932"));
                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                        return;
                    }
                    currentTime = Calendar.getInstance();
                    reports.add(0, new Report(df.format(currentTime.getTime()), "Contato de linha parada realizado com 982248932.\n"));
                    reportAdapter.notifyDataSetChanged();
                    reportDates.add(0, df.format(currentTime.getTime()));
                    reportDescriptions.add(0, "Contato de linha parada realizado com 982248932.\n");
                    startActivity(intent);


                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }

            else if (texto.contains("mais") &&
                    texto.contains("tempo") &&
                    (texto.contains("linha") || texto.contains("linhas")) &&
                    (texto.contains("parada") || texto.contains("paradas"))) {
                try {
                    Thread.sleep(5);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                ttobj.speak("A linha 05 está parada há 10 minutos"
                        , TextToSpeech.QUEUE_FLUSH, null);
            }

            else if
                    ((texto.contains("linha") || texto.contains("linhas")) &&
                    (texto.contains("parada") || texto.contains("paradas"))) {
                try {
                    Thread.sleep(5);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                ttobj.speak("A linha 05 está parada. O motivo da parada é falha no feeder do equipamento xp05."+
                                "A linha 03 está parada. O motivo da parada é falta de componentes no equipamento cb34."
                        , TextToSpeech.QUEUE_FLUSH, null);
                currentTime = Calendar.getInstance();
                reports.add(0, new Report(df.format(currentTime.getTime()), "A linha 05 está parada. O motivo da parada é falha no feeder do equipamento xp05.\n"+
                        "A linha 03 está parada. O motivo da parada é falta de componentes no equipamento cb34."));
                reportAdapter.notifyDataSetChanged();
                reportDates.add(0, df.format(currentTime.getTime()));
                reportDescriptions.add(0, "A linha 05 está parada. O motivo da parada é falha no feeder do equipamento xp05.\n"+
                        "A linha 03 está parada. O motivo da parada é falta de componentes no equipamento cb34.");
            }



            // ======================================================================== //
            // ======================================================================== //
            //                         Listar comandos disponíveis                      //
            // ======================================================================== //
            // ======================================================================== //
            else if (texto.contains("listar") &&
                    (texto.contains("comando") || texto.contains("comandos")) &&
                    (texto.contains("disponível") || texto.contains("disponíveis"))) {
                try {
                    Thread.sleep(5);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                ttobj.speak(
                        "Comando 1 : Linha com pior desempenho.\n"+
                                "Comando 2 : Análise perda de componente linha pior desempenho.\n"+
                                "Comando 3 : Relatório perda componente linha pior desempenho.\n"+
                                "Comando 4 : Contato perda componente linha pior desempenho.\n"+
                                "Comando 5 : Listar linhas paradas.\n"+
                                "Comando 6 : Relatório responsável linha parada.\n"+
                                "Comando 7 : Contato responsável linha parada.\n"+
                                "Comando 8 : Listar comandos disponíveis.\n",
                        TextToSpeech.QUEUE_FLUSH, null);
                currentTime = Calendar.getInstance();
                reports.add(0, new Report(df.format(currentTime.getTime()), "Comando 1 : Linha com pior desempenho.\n"+
                        "Comando 2 : Análise perda de componente linha pior desempenho.\n"+
                        "Comando 3 : Relatório perda componente linha pior desempenho.\n"+
                        "Comando 4 : Contato perda componente linha pior desempenho.\n"+
                        "Comando 5 : Listar linhas paradas.\n"+
                        "Comando 6 : Relatório responsável linha parada.\n"+
                        "Comando 7 : Contato responsável linha parada.\n"+
                        "Comando 8 : Listar comandos disponíveis.\n"));
                reportAdapter.notifyDataSetChanged();
                reportDates.add(0, df.format(currentTime.getTime()));
                reportDescriptions.add(0, "Comando 1 : Linha com pior desempenho.\n"+
                        "Comando 2 : Análise perda de componente linha pior desempenho.\n"+
                        "Comando 3 : Relatório perda componente linha pior desempenho.\n"+
                        "Comando 4 : Contato perda componente linha pior desempenho.\n"+
                        "Comando 5 : Listar linhas paradas.\n"+
                        "Comando 6 : Relatório responsável linha parada.\n"+
                        "Comando 7 : Contato responsável linha parada.\n"+
                        "Comando 8 : Listar comandos disponíveis.\n");
            }

            else {
                try {
                    ttobj.speak("Desculpa, näo entendi.", TextToSpeech.QUEUE_FLUSH, null);
                    Thread.sleep(3);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }


            }
        }
    }

    public void readMicrofone() {
        Intent intent = new Intent(
                RecognizerIntent.ACTION_RECOGNIZE_SPEECH);

        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, "en-US");

        try {
            startActivityForResult(intent, RESULT_SPEECH);

        } catch (ActivityNotFoundException a) {
            Toast t = Toast.makeText(this,
                    "Ops! Your device doesn't support Speech to Text",
                    Toast.LENGTH_SHORT);
            t.show();
        }
    }

}
