package com.example.lajos.shopfloor.network.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoseRawMaterialPojo {

    @SerializedName("idProduto")
    @Expose
    private Integer idProduto;
    @SerializedName("cdProduto")
    @Expose
    private String cdProduto;
    @SerializedName("corOcorrencia")
    @Expose
    private String corOcorrencia;
    @SerializedName("quantidadePerdida")
    @Expose
    private String quantidadePerdida;
    @SerializedName("porcentagemPerda")
    @Expose
    private String porcentagemPerda;
    @SerializedName("filtro")
    @Expose
    private FiltroPojo filtro;

    public Integer getIdProduto() {
        return idProduto;
    }

    public void setIdProduto(Integer idProduto) {
        this.idProduto = idProduto;
    }

    public String getCdProduto() {
        return cdProduto;
    }

    public void setCdProduto(String cdProduto) {
        this.cdProduto = cdProduto;
    }

    public String getCorOcorrencia() {
        return corOcorrencia;
    }

    public void setCorOcorrencia(String corOcorrencia) {
        this.corOcorrencia = corOcorrencia;
    }

    public String getQuantidadePerdida() {
        return quantidadePerdida;
    }

    public void setQuantidadePerdida(String quantidadePerdida) {
        this.quantidadePerdida = quantidadePerdida;
    }

    public String getPorcentagemPerda() {
        return porcentagemPerda;
    }

    public void setPorcentagemPerda(String porcentagemPerda) {
        this.porcentagemPerda = porcentagemPerda;
    }

    public FiltroPojo getFiltro() {
        return filtro;
    }

    public void setFiltro(FiltroPojo filtro) {
        this.filtro = filtro;
    }

}
