package com.example.lajos.shopfloor.network.pojo;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WorkGroupResponse {

    @SerializedName("gts")
    @Expose
    private List<WorkGroupPojo> gts = null;

    public List<WorkGroupPojo> getGts() {
        return gts;
    }

    public void setGts(List<WorkGroupPojo> gts) {
        this.gts = gts;
    }

}