package com.example.lajos.shopfloor.network.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserLoginResponse {

    @SerializedName("cdUsr")
    @Expose
    private String cdUsr;
    @SerializedName("token")
    @Expose
    private String token;
    @SerializedName("login")
    @Expose
    private String login;
    @SerializedName("dsApelido")
    @Expose
    private String dsApelido;
    @SerializedName("cdGtMonitorizacao")
    @Expose
    private String cdGtMonitorizacao;

    public String getCdUsr() {
        return cdUsr;
    }

    public void setCdUsr(String cdUsr) {
        this.cdUsr = cdUsr;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getDsApelido() {
        return dsApelido;
    }

    public void setDsApelido(String dsApelido) {
        this.dsApelido = dsApelido;
    }

    public String getCdGtMonitorizacao() {
        return cdGtMonitorizacao;
    }

    public void setCdGtMonitorizacao(String cdGtMonitorizacao) {
        this.cdGtMonitorizacao = cdGtMonitorizacao;
    }

}