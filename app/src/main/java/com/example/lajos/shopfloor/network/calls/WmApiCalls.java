package com.example.lajos.shopfloor.network.calls;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.speech.tts.TextToSpeech;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import com.example.lajos.shopfloor.activity.MainActivity;
import com.example.lajos.shopfloor.activity.MasterActivity;
import com.example.lajos.shopfloor.adapter.RawMaterialLoseItemAdapter;
import com.example.lajos.shopfloor.adapter.ReportAdapter;
import com.example.lajos.shopfloor.adapter.WorkGroupAdapter;
import com.example.lajos.shopfloor.adapter.WorkGroupMonitorizationAdapter;
import com.example.lajos.shopfloor.adapter.WorkgroupSpinAdapter;
import com.example.lajos.shopfloor.data.Report;
import com.example.lajos.shopfloor.data.WorkGroup;
import com.example.lajos.shopfloor.network.pojo.ActualTurnResponse;
import com.example.lajos.shopfloor.network.pojo.AnalyzesFilterPost;
import com.example.lajos.shopfloor.network.pojo.AnalyzesFilterPostPojo;
import com.example.lajos.shopfloor.network.pojo.AnalyzesResponse;
import com.example.lajos.shopfloor.network.pojo.AnalyzesWorstRawMaterialFilterPost;
import com.example.lajos.shopfloor.network.pojo.AnalyzesWorstRawMaterialPostPojo;
import com.example.lajos.shopfloor.network.pojo.FilterDetailPojo;
import com.example.lajos.shopfloor.network.pojo.LoseRawMaterialPojo;
import com.example.lajos.shopfloor.network.pojo.MonitorizationResponse;
import com.example.lajos.shopfloor.network.pojo.UserLoginPojo;
import com.example.lajos.shopfloor.network.pojo.UserLoginResponse;
import com.example.lajos.shopfloor.network.pojo.WorkGroupMonitorizationPojo;
import com.example.lajos.shopfloor.network.pojo.WorkGroupPojo;
import com.example.lajos.shopfloor.network.pojo.WorkGroupResponse;
import com.example.lajos.shopfloor.network.service.WmService;
import com.example.lajos.shopfloor.util.MathOperations;
import com.example.lajos.shopfloor.util.UserSession;
import com.leo.simplearcloader.ArcConfiguration;
import com.leo.simplearcloader.SimpleArcDialog;

import org.angmarch.views.NiceSpinner;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class WmApiCalls {

    private final Context context;
    private final SimpleArcDialog loadingDialog;
    private final Activity activity;
    private final WmService service;
    private final MathOperations mathOperations = new MathOperations();
    private final Integer FILTER_OP = 0;
    private final Boolean ACTUAL_TURN = false;
    private final String CALL_FAILURE = "Fail, please check your connection.";
    private final String CALL_404 = "404, Not found.";
    private final String CALL_FAILURE_LOGIN = "Erro : Verifique os seus dados de login e sua conexão";

    public WmApiCalls(Context context, Activity activity) {
        this.context = context;
        this.activity = activity;
        this.loadingDialog = new SimpleArcDialog(this.activity);
        ArcConfiguration config = new ArcConfiguration(this.activity);
        config.setText("Carregando...");
        loadingDialog.setConfiguration(config);
        loadingDialog.setCancelable(false);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(WmService.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        this.service = retrofit.create(WmService.class);
    }

    // ======================================================================== //
    // ======================================================================== //
    //               API call to perform user login on VF platform              //
    // ======================================================================== //
    // ======================================================================== //
    public void userLogin(final String username,
                          final String password,
                          final UserSession session,
                          final TextToSpeech ttobj){

        loadingDialog.show();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(WmService.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        final WmService service = retrofit.create(WmService.class);
        final Call<UserLoginResponse> requestStocks = service.postUserLogin(new UserLoginPojo(username, password));
        requestStocks.enqueue(new Callback<UserLoginResponse>() {
            @Override
            public void onResponse(@NonNull Call<UserLoginResponse> call, @NonNull Response<UserLoginResponse> response) {
                if (!response.isSuccessful()) {
                    if (response.code() == 404 || response.code() == 400) {
                        loadingDialog.hide();
                        loadingDialog.dismiss();
                        Toast.makeText(context, CALL_FAILURE_LOGIN, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    UserLoginResponse fetchedData = response.body();
                    session.login(Objects.requireNonNull(fetchedData).getLogin(), fetchedData.getToken());
                    checkLoginStatus(session, ttobj);
                }
            }

            @Override
            public void onFailure(@NonNull Call<UserLoginResponse> call, @NonNull Throwable t) {
                Log.e("Retrofit", "ERROR" + t.getMessage());
                loadingDialog.hide();
                loadingDialog.dismiss();
                Toast.makeText(context, "Erro : Verifique os seus dados de login e sua conexão", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void checkLoginStatus(UserSession session, final TextToSpeech ttobj){
        if(session.checkLoginStatus()){
            loadingDialog.hide();
            loadingDialog.dismiss();
            Intent intent = new Intent(activity, MasterActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
            activity.finish();
        }
        else{
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    //ttobj.speak("Boa tarde! Bem vindo ao WM. Por favor, informe seu usuário e senha.", TextToSpeech.QUEUE_FLUSH, null);
                }
            }, 1000);
        }
    }

    // ======================================================================== //
    // ======================================================================== //
    //                API call to load all available workgroups                 //
    // ======================================================================== //
    // ======================================================================== //
    public void loadWorkGroups(final ArrayList<WorkGroup> mGroups,
                               final WorkGroupAdapter workGroupAdapter){

        loadingDialog.show();

        final Call<WorkGroupResponse> requestWorkGroups = service.getWorkGroups();
        requestWorkGroups.enqueue(new Callback<WorkGroupResponse>() {
            @Override
            public void onResponse(@NonNull Call<WorkGroupResponse> call, @NonNull Response<WorkGroupResponse> response) {
                if (!response.isSuccessful()){
                    if(response.code() == 404){ Toast.makeText(context, CALL_404, Toast.LENGTH_SHORT).show(); }
                }
                else{
                    List<WorkGroupPojo> fetchedData = Objects.requireNonNull(response.body()).getGts();
                    for(WorkGroupPojo wg : fetchedData){
                        mGroups.add(new WorkGroup(wg.getDsGt(), wg.getCdGt()));
                    }
                    workGroupAdapter.notifyDataSetChanged();
                    loadingDialog.hide();
                    loadingDialog.dismiss();
                }
            }
            @Override
            public void onFailure(@NonNull Call<WorkGroupResponse> call, @NonNull Throwable t) {
                Toast.makeText(context, CALL_FAILURE, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void loadWorkGroupsSpinner(final ArrayList<WorkGroup> mGroups,
                                      final NiceSpinner workgroupSpinner,
                                      final String token,
                                      final ArrayList<WorkGroupMonitorizationPojo> wgs,
                                      final ArrayList<WorkGroupMonitorizationPojo> selectedGroupLinesPerformanceOrder,
                                      final WorkGroupMonitorizationPojo worstGroupLinePerformance,
                                      final WorkGroupMonitorizationAdapter wgsAdapter){

        loadingDialog.show();

        final Call<WorkGroupResponse> requestWorkGroups = service.getWorkGroups();
        requestWorkGroups.enqueue(new Callback<WorkGroupResponse>() {
            @Override
            public void onResponse(@NonNull Call<WorkGroupResponse> call, @NonNull Response<WorkGroupResponse> response) {
                if (!response.isSuccessful()){
                    if(response.code() == 404){ Toast.makeText(context, CALL_404, Toast.LENGTH_SHORT).show(); }
                }
                else{
                    List<WorkGroupPojo> fetchedData = Objects.requireNonNull(response.body()).getGts();
                    for(WorkGroupPojo wg : fetchedData){
                        mGroups.add(new WorkGroup(wg.getDsGt(), wg.getCdGt()));
                    }
                    workgroupSpinner.attachDataSource(mGroups);
                    getActualTurnSpinner(mGroups.get(0).getWorkGroupCode(),
                            token,
                            wgs,
                            selectedGroupLinesPerformanceOrder,
                            worstGroupLinePerformance,
                            wgsAdapter);
                }
            }
            @Override
            public void onFailure(@NonNull Call<WorkGroupResponse> call, @NonNull Throwable t) {
                Toast.makeText(context, CALL_FAILURE, Toast.LENGTH_SHORT).show();
            }
        });
    }



    // ======================================================================== //
    // ======================================================================== //
    //        API call to load actual turn and load monitorization info         //
    //              based on actual turn and selected workgroup                 //
    // ======================================================================== //
    // ======================================================================== //
    public void getActualTurn(final String workgroup_code,
                              final String token,
                              final ArrayList<WorkGroupMonitorizationPojo> wgs,
                              final WorkGroupMonitorizationAdapter wgsAdapter){

        loadingDialog.show();

        final Call<ActualTurnResponse> requestWorkGroups = service.getActualTurn(workgroup_code, token);
        requestWorkGroups.enqueue(new Callback<ActualTurnResponse>() {
            @Override
            public void onResponse(@NonNull Call<ActualTurnResponse> call, @NonNull Response<ActualTurnResponse> response) {
                if (!response.isSuccessful()){
                    if(response.code() == 404){ Toast.makeText(context, CALL_404, Toast.LENGTH_SHORT).show(); }
                }
                else{
                    ActualTurnResponse actualTurnResponse = response.body();
                    FilterDetailPojo filter = new FilterDetailPojo(
                            Objects.requireNonNull(actualTurnResponse).getDtReferencia(),
                            actualTurnResponse.getIdTurno(),
                            FILTER_OP,
                            workgroup_code,
                            ACTUAL_TURN);
                    getMonitorization(token, filter, wgs, wgsAdapter);
                }
            }
            @Override
            public void onFailure(@NonNull Call<ActualTurnResponse> call, @NonNull Throwable t) {
                Toast.makeText(context, CALL_FAILURE, Toast.LENGTH_SHORT).show();
            }
        });
    }


    public void getActualTurnSpinner(final String workgroup_code,
                                     final String token,
                                     final ArrayList<WorkGroupMonitorizationPojo> wgs,
                                     final ArrayList<WorkGroupMonitorizationPojo> selectedGroupLinesPerformanceOrder,
                                     final WorkGroupMonitorizationPojo worstGroupLinePerformance,
                                     final WorkGroupMonitorizationAdapter wgsAdapter){

        loadingDialog.show();

        final Call<ActualTurnResponse> requestWorkGroups = service.getActualTurn(workgroup_code, token);
        requestWorkGroups.enqueue(new Callback<ActualTurnResponse>() {
            @Override
            public void onResponse(@NonNull Call<ActualTurnResponse> call, @NonNull Response<ActualTurnResponse> response) {
                if (!response.isSuccessful()){
                    if(response.code() == 404){ Toast.makeText(context, CALL_404, Toast.LENGTH_SHORT).show(); }
                }
                else{
                    ActualTurnResponse actualTurnResponse = response.body();
                    FilterDetailPojo filter = new FilterDetailPojo(
                            //Objects.requireNonNull(actualTurnResponse).getDtReferencia(),
                            "16/07/2018",
                            //actualTurnResponse.getIdTurno(),
                            2,
                            //FILTER_OP,
                            0,
                            workgroup_code,
                            //ACTUAL_TURN);
                            false);
                    getMonitorizationSpinner(token,
                            filter,
                            wgs,
                            selectedGroupLinesPerformanceOrder,
                            worstGroupLinePerformance,
                            wgsAdapter);
                }
            }
            @Override
            public void onFailure(@NonNull Call<ActualTurnResponse> call, @NonNull Throwable t) {
                Toast.makeText(context, CALL_FAILURE, Toast.LENGTH_SHORT).show();
            }
        });
    }

    // ======================================================================== //
    // ======================================================================== //
    //        API call to load workgroups based on previous actual turn         //
    // loaded through getActualTurn() method. This is called by getActualTurn() //
    // ======================================================================== //
    // ======================================================================== //
    private void getMonitorization(final String token,
                                  final FilterDetailPojo filter,
                                  final ArrayList<WorkGroupMonitorizationPojo> wgs,
                                  final WorkGroupMonitorizationAdapter wgsAdapter){

        final Call<MonitorizationResponse> requestWorkGroups = service.getMonitorization(filter, token);
        requestWorkGroups.enqueue(new Callback<MonitorizationResponse>() {
            @Override
            public void onResponse(@NonNull Call<MonitorizationResponse> call, @NonNull Response<MonitorizationResponse> response) {
                if (!response.isSuccessful()){
                    if(response.code() == 404){ Toast.makeText(context, CALL_404, Toast.LENGTH_SHORT).show(); }
                }
                else{
                    wgs.addAll(Objects.requireNonNull(response.body()).getGts());
                    wgsAdapter.notifyDataSetChanged();
                    loadingDialog.hide();
                    loadingDialog.dismiss();
                }
            }
            @Override
            public void onFailure(@NonNull Call<MonitorizationResponse> call, @NonNull Throwable t) {
                Toast.makeText(context, CALL_FAILURE, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getMonitorizationSpinner(final String token,
                                          final FilterDetailPojo filter,
                                          final ArrayList<WorkGroupMonitorizationPojo> wgs,
                                          final ArrayList<WorkGroupMonitorizationPojo> selectedGroupLinesPerformanceOrder,
                                          final WorkGroupMonitorizationPojo worstGroupLinePerformance,
                                          final WorkGroupMonitorizationAdapter wgsAdapter){

        //filter.setDtReferencia("12/07/2018");
        final Call<MonitorizationResponse> requestWorkGroups = service.getMonitorization(filter, token);
        requestWorkGroups.enqueue(new Callback<MonitorizationResponse>() {
            @Override
            public void onResponse(@NonNull Call<MonitorizationResponse> call, @NonNull Response<MonitorizationResponse> response) {
                if (!response.isSuccessful()){
                    if(response.code() == 404){ Toast.makeText(context, CALL_404, Toast.LENGTH_SHORT).show(); }
                }
                else{
                    wgs.addAll(Objects.requireNonNull(response.body()).getGts());
                    selectedGroupLinesPerformanceOrder.addAll(response.body().getGts());
                    Collections.sort(selectedGroupLinesPerformanceOrder, new Comparator<WorkGroupMonitorizationPojo>() {
                        @Override
                        public int compare(WorkGroupMonitorizationPojo t1, WorkGroupMonitorizationPojo t2) {
                            Float t1_float = Float.valueOf(t1.getIndiceOEE());
                            Float t2_float = Float.valueOf(t2.getIndiceOEE());
                            return Math.round(t1_float-t2_float);
                        }
                    });
                    mathOperations.removeZeroPerformance(selectedGroupLinesPerformanceOrder);
                    wgsAdapter.notifyDataSetChanged();
                    loadingDialog.hide();
                }
            }
            @Override
            public void onFailure(@NonNull Call<MonitorizationResponse> call, @NonNull Throwable t) {
                Toast.makeText(context, CALL_FAILURE, Toast.LENGTH_SHORT).show();
            }
        });
    }

    // ======================================================================== //
    // ======================================================================== //
    //       Load all component loses based on a previously selected line       //
    // ======================================================================== //
    // ======================================================================== //
    public void getAnalyzes(final AnalyzesFilterPost analyzesFilterPost,
                            final String token,
                            final ArrayList<LoseRawMaterialPojo> rawLoses,
                            final RawMaterialLoseItemAdapter rawLoseAdapter){

        loadingDialog.show();

        analyzesFilterPost.getFiltro().setDtReferencia("16/07/2018");

        final Call<AnalyzesResponse> requestAnalyzes = service.getAnalyzes(analyzesFilterPost, token);
        requestAnalyzes.enqueue(new Callback<AnalyzesResponse>() {
            @Override
            public void onResponse(@NonNull Call<AnalyzesResponse> call, @NonNull Response<AnalyzesResponse> response) {
                if(response.code() != 200){
                    Toast.makeText(context, CALL_404, Toast.LENGTH_SHORT).show();
                }
                else{
                    AnalyzesResponse fetchedData = response.body();
                    rawLoses.addAll(Objects.requireNonNull(fetchedData).getPerdasMateriaPrima());
                    rawLoseAdapter.notifyDataSetChanged();
                    loadingDialog.hide();
                    loadingDialog.dismiss();
                }
            }
            @Override
            public void onFailure(@NonNull Call<AnalyzesResponse> call, @NonNull Throwable t) {
                Toast.makeText(context, CALL_FAILURE, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void getMostLossRawMaterial(final AnalyzesFilterPost analyzesFilterPost,
                                       final String token,
                                       final TextToSpeech ttobj,
                                       final ArrayList<Report> reports,
                                       final ReportAdapter reportAdapter,
                                       final Calendar currentTime,
                                       final SimpleDateFormat df,
                                       final ArrayList<String> reportDescriptions,
                                       final ArrayList<String> reportDates){

        loadingDialog.show();
        analyzesFilterPost.getFiltro().setDtReferencia("16/07/2018");
        final ArrayList<LoseRawMaterialPojo> rawLoses = new ArrayList<>();

        final Call<AnalyzesResponse> requestAnalyzes = service.getAnalyzes(analyzesFilterPost, token);
        requestAnalyzes.enqueue(new Callback<AnalyzesResponse>() {
            @Override
            public void onResponse(@NonNull Call<AnalyzesResponse> call, @NonNull Response<AnalyzesResponse> response) {
                if(response.code() != 200){
                    Toast.makeText(context, CALL_404, Toast.LENGTH_SHORT).show();
                }
                else{
                    AnalyzesResponse fetchedData = response.body();
                    rawLoses.addAll(Objects.requireNonNull(fetchedData).getPerdasMateriaPrima());
                    LoseRawMaterialPojo mostLossRawMaterial = rawLoses.get(0);
                    String materialName[] = mostLossRawMaterial.getCdProduto().split(" ");
//                    loadingDialog.hide();
//                    loadingDialog.dismiss();
                    String talk = "Maior perda de componente:\nComponente "+materialName[0]+
                            "\nQuantidade perdida "+mostLossRawMaterial.getQuantidadePerdida()+
                            "\nPorcentagem da perda "+mostLossRawMaterial.getPorcentagemPerda()+"%";
                    getMostLostRawMaterialTool(mostLossRawMaterial, token, talk, ttobj, reports, reportAdapter, currentTime, df, reportDates, reportDescriptions);
//                    ttobj.speak("Maior perda de componente. Componente "+materialName[0]+
//                            ". Quantidade perdida "+mostLossRawMaterial.getQuantidadePerdida(),
//                            TextToSpeech.QUEUE_FLUSH,
//                            null);
                }
            }
            @Override
            public void onFailure(@NonNull Call<AnalyzesResponse> call, @NonNull Throwable t) {
                Toast.makeText(context, CALL_FAILURE, Toast.LENGTH_SHORT).show();
            }
        });
    }

    // ======================================================================== //
    // ======================================================================== //
    //             Load the tools using the most lost raw material              //
    // ======================================================================== //
    // ======================================================================== //
    public void getMostLostRawMaterialTool(final LoseRawMaterialPojo mostLossRawMaterial,
                                           final String token,
                                           final String talk,
                                           final TextToSpeech ttobj,
                                           final ArrayList<Report> reports,
                                           final ReportAdapter reportAdapter,
                                           final Calendar currentTime,
                                           final SimpleDateFormat df,
                                           final ArrayList<String> reportDescriptions,
                                           final ArrayList<String> reportDates){

        //loadingDialog.show();
        final ArrayList<LoseRawMaterialPojo> rawLoses = new ArrayList<>();

        AnalyzesWorstRawMaterialPostPojo analyzesWorstRawMaterialPostPojo = new AnalyzesWorstRawMaterialPostPojo(
                0,
                0,
                "16/07/2018",
                2,
                "CP4L06",
                1,
                18463,
                0,
                0);
        AnalyzesWorstRawMaterialFilterPost analyzesWorstRawMaterialFilterPost = new AnalyzesWorstRawMaterialFilterPost(analyzesWorstRawMaterialPostPojo);


        final Call<AnalyzesResponse> requestAnalyzes = service.getWorstRawMaterialTool(analyzesWorstRawMaterialFilterPost, token);
        requestAnalyzes.enqueue(new Callback<AnalyzesResponse>() {
            @Override
            public void onResponse(@NonNull Call<AnalyzesResponse> call, @NonNull Response<AnalyzesResponse> response) {
                if(response.code() != 200){
                    Toast.makeText(context, CALL_404, Toast.LENGTH_SHORT).show();
                }
                else{
                    AnalyzesResponse fetchedData = response.body();
                    ttobj.speak(talk+"\nMáquina com maior perda "+fetchedData.getPerdasFerramenta().get(0).getCdFerramenta(),TextToSpeech.QUEUE_FLUSH, null);
                    reports.add(0, new Report(df.format(currentTime.getTime()), talk+"\nMáquina com maior perda "+fetchedData.getPerdasFerramenta().get(0).getCdFerramenta()));
                    reportAdapter.notifyDataSetChanged();
                    loadingDialog.hide();

                }
            }
            @Override
            public void onFailure(@NonNull Call<AnalyzesResponse> call, @NonNull Throwable t) {
                Toast.makeText(context, CALL_FAILURE, Toast.LENGTH_SHORT).show();
            }
        });
    }

}
