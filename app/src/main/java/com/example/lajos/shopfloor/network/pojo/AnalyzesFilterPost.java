package com.example.lajos.shopfloor.network.pojo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AnalyzesFilterPost {

    @SerializedName("filtro")
    @Expose
    private AnalyzesFilterPostPojo filtro;

    public AnalyzesFilterPost(AnalyzesFilterPostPojo filtro) {
        this.filtro = filtro;
    }

    public AnalyzesFilterPostPojo getFiltro() {
        return filtro;
    }

    public void setFiltro(AnalyzesFilterPostPojo filtro) {
        this.filtro = filtro;
    }

}