package com.example.lajos.shopfloor.network.service;

import com.example.lajos.shopfloor.network.pojo.ActualTurnResponse;
import com.example.lajos.shopfloor.network.pojo.AnalyzesFilterPost;
import com.example.lajos.shopfloor.network.pojo.AnalyzesResponse;
import com.example.lajos.shopfloor.network.pojo.AnalyzesWorstRawMaterialFilterPost;
import com.example.lajos.shopfloor.network.pojo.FilterDetailPojo;
import com.example.lajos.shopfloor.network.pojo.MonitorizationResponse;
import com.example.lajos.shopfloor.network.pojo.UserLoginPojo;
import com.example.lajos.shopfloor.network.pojo.UserLoginResponse;
import com.example.lajos.shopfloor.network.pojo.WorkGroupResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface WmService {

    String BASE_URL = "http://vfactory.flex-am.com.br:8080/idw/rest/";

    @Headers("content-type: application/json")
    @POST("/idw/rest/usuarios/logar")
    Call<UserLoginResponse> postUserLogin(@Body UserLoginPojo body);

    @Headers("content-type: application/json")
    @GET("/idw/rest/monitorizacao/turnoAtual")
    Call<ActualTurnResponse> getActualTurn(@Query("cdGt") String workgroup_code, @Header("Authentication") String token);

    @GET("/idw/rest/gts/monitorizacao")
    Call<WorkGroupResponse> getWorkGroups();

    @Headers("content-type: application/json")
    @POST("/idw/rest/monitorizacao")
    Call<MonitorizationResponse> getMonitorization(@Body FilterDetailPojo body, @Header("Authentication") String token);

    @Headers("content-type: application/json")
    @POST("/idw/rest/monitorizacao/detalhe/materiaPrima")
    Call<AnalyzesResponse> getAnalyzes(@Body AnalyzesFilterPost body, @Header("Authentication") String token);

    @Headers("content-type: application/json")
    @POST("/idw/rest/monitorizacao/detalhe/materiaPrima")
    Call<AnalyzesResponse> getWorstRawMaterialTool(@Body AnalyzesWorstRawMaterialFilterPost body, @Header("Authentication") String token);

}
