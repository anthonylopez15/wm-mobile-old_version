package com.example.lajos.shopfloor.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.lajos.shopfloor.R;
import com.example.lajos.shopfloor.activity.MonitorizationActivity;
import com.example.lajos.shopfloor.data.WorkGroup;

import java.util.ArrayList;

public class WorkGroupAdapter extends RecyclerView.Adapter<WorkGroupAdapter.WorkGroupHolder> {

    private final ArrayList<WorkGroup> mGroups;

    public WorkGroupAdapter(ArrayList<WorkGroup> mGroups) {
        this.mGroups = mGroups;
    }

    @NonNull
    @Override
    public WorkGroupHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new WorkGroupHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.workgroup_card, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull WorkGroupAdapter.WorkGroupHolder holder, int position) {
        final WorkGroup wg = mGroups.get(position);
        holder.workGroupName.setText(wg.getWorkGroupName());
        holder.workGroupCode.setText(wg.getWorkGroupCode());

        holder.card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Context context = view.getContext();
                Intent intent = new Intent(context, MonitorizationActivity.class);
                intent.putExtra("workgroup_code", wg.getWorkGroupCode());
                intent.putExtra("workgroup_name", wg.getWorkGroupName());
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mGroups.size();
    }

    public class WorkGroupHolder extends RecyclerView.ViewHolder{

        CardView card;
        private TextView workGroupName;
        private TextView workGroupCode;

        public WorkGroupHolder(View itemView) {
            super(itemView);
            card = itemView.findViewById(R.id.card_view);
            workGroupName = itemView.findViewById(R.id.workgroup_name);
            workGroupCode = itemView.findViewById(R.id.workgroup_code);
        }
    }
}
