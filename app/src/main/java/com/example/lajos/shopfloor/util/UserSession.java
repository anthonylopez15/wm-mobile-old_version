package com.example.lajos.shopfloor.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class UserSession {

    public SharedPreferences preferences;
    public Editor editor;
    public static final String MY_PREF = "myPref";
    public static final String IS_LOGGED = "isLogged";
    public static final String USER_NAME = "username";
    public static final String TOKEN = "token";

    public UserSession(Context applicationContext) {
        this.preferences = applicationContext.getSharedPreferences(MY_PREF,0);
        this.editor = preferences.edit();
    }

    public void login(String username, String token){
        editor.putString(USER_NAME, username);
        editor.putString(TOKEN, token);
        editor.putInt(IS_LOGGED, 1);
        editor.commit();
    }

    public void logout(){
        editor.clear();
        editor.commit();
    }

    public boolean checkLoginStatus(){
        return preferences.contains(IS_LOGGED);
    }

    public String getUsername (){
        return preferences.getString(USER_NAME, "");
    }

    public String getToken() { return preferences.getString(TOKEN, ""); }
}
