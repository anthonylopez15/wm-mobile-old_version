package com.example.lajos.shopfloor.activity;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Handler;
import android.speech.RecognizerIntent;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.lajos.shopfloor.R;
import com.example.lajos.shopfloor.adapter.WorkGroupAdapter;
import com.example.lajos.shopfloor.data.WorkGroup;
import com.example.lajos.shopfloor.network.calls.WmApiCalls;
import com.example.lajos.shopfloor.util.UserSession;

import java.util.ArrayList;
import java.util.Locale;


public class MainActivity extends AppCompatActivity {

    private ArrayList<WorkGroup> mGroups = new ArrayList<>();
    WorkGroupAdapter workGroupAdapter;
    GridLayoutManager layoutManager;
    RecyclerView workGroupRecycler;
    UserSession session;
    Context context;

    static int teste = 0;


    protected static TextToSpeech ttobj;
    protected static final int RESULT_SPEECH = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Context context = getApplicationContext();
        session = new UserSession(getApplicationContext());
        WmApiCalls calls = new WmApiCalls(context, MainActivity.this);
        context = getApplicationContext();

        Toolbar toolbar = findViewById(R.id.toolbar);
        ImageButton voiceButton = findViewById(R.id.voice_button);
        workGroupRecycler = findViewById(R.id.workgroup_recycler);
        layoutManager = new GridLayoutManager(this,3);
        workGroupAdapter = new WorkGroupAdapter(mGroups);

        toolbar.setTitle(R.string.wm_linhas_cadastradas );
        setSupportActionBar(toolbar);

        workGroupRecycler.setLayoutManager(layoutManager);
        workGroupRecycler.setAdapter(workGroupAdapter);

        calls.loadWorkGroups(mGroups, workGroupAdapter);

        ttobj = new TextToSpeech(getApplicationContext(),
                new TextToSpeech.OnInitListener() {
                    @Override
                    public void onInit(int status) {
                        if (status != TextToSpeech.ERROR) {
                            ttobj.setLanguage(new Locale("pt", "br"));
                        }
                    }
                });

        voiceButton.setOnClickListener(new View.OnClickListener() {
            //
            @Override
            public void onClick(View v) {
                if (teste == 0) {
                    teste = 1;
                    //ttobj.speak("Olá, Você gostaria de uma análise de balanceamento ou uma análise por perda de componente?", TextToSpeech.QUEUE_FLUSH, null);
                }
                readMicrofone();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.toolbar_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.logout:
                session.logout();
                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(intent);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case RESULT_SPEECH: {
                if (resultCode == RESULT_OK && null != data) {
                    ArrayList<String> text = data
                            .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    String texto = text.get(0);
                    falar(texto);
                }
                break;
            }
        }
    }


    public void falar(String texto) {
        if (texto != null && texto.isEmpty() == false) {

            if (texto.contains("perda")) {

                // String nova = texto.replaceAll(" \\S*$", texto.substring(texto.lastIndexOf(" ")+1).replace(""," "));
                //ttobj.speak(texto.substring(0, texto.lastIndexOf(" ")) + texto.substring(texto.lastIndexOf(" ") + 1).replace("", " "), TextToSpeech.QUEUE_FLUSH, null);
                try {
                    Thread.sleep(5);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                // ttobj.speak("A principal perda de componente está na Linha 09. Você gostaria de uma análise de balanceamento ou uma análise por perda de componente? ", TextToSpeech.QUEUE_FLUSH, null);
                ttobj.speak("O componente xyz123 na máquina cp3,apresenta problema no feeder 5n343 na posição 72. É necessário Trocá-lo imediatamente.", TextToSpeech.QUEUE_FLUSH, null);
            } else if (texto.contains("pior desempenho")) {
                try {
                    Thread.sleep(5);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                ttobj.speak("A linha com pior desempenho OEE é a Linha 09 . Você gostaria de uma análise de balanceamento ou uma análise por perda de componente? ", TextToSpeech.QUEUE_FLUSH, null);
            }


            else if (texto.contains("")) {
                try {
                    Thread.sleep(5);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                ttobj.speak("Abrindo o grupo de trabalho de testes da MAP", TextToSpeech.QUEUE_FLUSH, null);
                Intent intent = new Intent(this, MonitorizationActivity.class);
                intent.putExtra("workgroup_code", "FABMAP");
                intent.putExtra("workgroup_name", "Grupo de testes da MAP");
                this.startActivity(intent);
            }



//            else if (texto.contains("causa")) {
//
//                // String nova = texto.replaceAll(" \\S*$", texto.substring(texto.lastIndexOf(" ")+1).replace(""," "));
//                //ttobj.speak(texto.substring(0, texto.lastIndexOf(" ")) + texto.substring(texto.lastIndexOf(" ") + 1).replace("", " "), TextToSpeech.QUEUE_FLUSH, null);
//
//
//                try {
//                    Thread.sleep(5);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//
//                ttobj.speak("A principal causa é o problema no Feeder", TextToSpeech.QUEUE_FLUSH, null);
//            }
            else {
                try {
                    ttobj.speak("Abrindo o grupo de trabalho de testes da MAP", TextToSpeech.QUEUE_FLUSH, null);
                    Intent intent = new Intent(this, MonitorizationActivity.class);
                    intent.putExtra("workgroup_code", "FABMAP");
                    intent.putExtra("workgroup_name", "Grupo de testes da MAP");
                    this.startActivity(intent);
                    Thread.sleep(3);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }


            }
            //ttobj.speak(texto, TextToSpeech.QUEUE_FLUSH, null);
        }
    }

    public void readMicrofone() {
        Intent intent = new Intent(
                RecognizerIntent.ACTION_RECOGNIZE_SPEECH);

        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, "en-US");

        try {
            startActivityForResult(intent, RESULT_SPEECH);

        } catch (ActivityNotFoundException a) {
            Toast t = Toast.makeText(this,
                    "Ops! Your device doesn't support Speech to Text",
                    Toast.LENGTH_SHORT);
            t.show();
        }
    }

}
