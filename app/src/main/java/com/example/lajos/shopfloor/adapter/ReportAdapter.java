package com.example.lajos.shopfloor.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.lajos.shopfloor.R;
import com.example.lajos.shopfloor.data.Report;

import java.util.ArrayList;


public class ReportAdapter extends RecyclerView.Adapter<ReportAdapter.ReportViewHolder> {

    ArrayList<Report> mReports;

    public ReportAdapter(ArrayList<Report> mReports) {
        this.mReports = mReports;
    }

    @NonNull
    @Override
    public ReportViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ReportAdapter.ReportViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.report_card, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ReportViewHolder holder, int position) {
        final Report report = mReports.get(position);
        holder.date.setText(report.getDate());
        holder.description.setText(report.getDescription());
    }

    @Override
    public int getItemCount() {
        return mReports.size();
    }

    public class ReportViewHolder extends RecyclerView.ViewHolder{


        TextView date;
        TextView description;

        public ReportViewHolder(View itemView) {
            super(itemView);
            date = itemView.findViewById(R.id.report_date_text);
            description = itemView.findViewById(R.id.report_description_text);
        }
    }
}
